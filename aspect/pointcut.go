package aspect

import (
	"reflect"
	"unsafe"
)

func Inject(o interface{}) {
	t := reflect.TypeOf(o).Elem()
	v := reflect.ValueOf(o).Elem()
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		value := v.Field(i)
		value = reflect.NewAt(value.Type(), unsafe.Pointer(value.UnsafeAddr())).Elem() // use unsafe to access unexported field
		tag := field.Tag
		before := tag.Get("before")
		if len(before) > 0 {
			advice, ok := Advice[before]
			if ok {
				cloneValue := value.Interface().(func())
				newValue := reflect.ValueOf(func() {
					advice()
					cloneValue()
				})
				value.Set(newValue)
			}
		}
		after := tag.Get("after")
		if len(after) > 0 {
			advice, ok := Advice[after]
			if ok {
				cloneValue := value.Interface().(func())
				newValue := reflect.ValueOf(func() {
					cloneValue()
					advice()
				})
				value.Set(newValue)
			}
		}
	}
}
