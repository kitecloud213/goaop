package aspect

import (
	"fmt"
	"time"
)

var Advice map[string]func()

func init() {
	Advice = make(map[string]func())
	Advice["LogTime"] = LogTime
	Advice["SayHi"] = SayHi
}

func LogTime() {
	fmt.Println(time.Now())
}

func SayHi() {
	fmt.Println("Hi")
}
