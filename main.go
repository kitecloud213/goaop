package main

import (
	"fmt"
	"gitlab.com/kitecloud213/goaop/aspect"
)

func main() {
	var o Obj
	o = NewObj()
	o.Method()
}

type Obj interface {
	Method()
}

type obj struct {
	method func() `before:"SayHi" after:"LogTime"`
}

func NewObj() *obj {
	o := &obj{}
	o.method = func() {
		fmt.Printf("[%p] method invoke!\n", o)
	}

	aspect.Inject(o)

	return o
}

func (o *obj) Method() {
	o.method()
}
